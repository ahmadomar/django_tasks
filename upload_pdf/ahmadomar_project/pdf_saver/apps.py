from django.apps import AppConfig


class PdfSaverConfig(AppConfig):
    name = 'pdf_saver'
