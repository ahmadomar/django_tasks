from django.shortcuts import render
from .models import Upload_pdf,Pdf_information
from django.core.files.storage import FileSystemStorage
from .forms import pdf_Form
# Create your views here.
from django.template import loader
from django.http import HttpResponse,HttpResponseRedirect
from django.conf import settings
from base64 import b64decode ,b64encode
import os
from collections import ChainMap
from pdfminer.pdfparser import PDFParser
from pdfminer.pdfdocument import PDFDocument
pdfs_type = ['pdf']
new_extension=".txt"
from rest_framework.views   import APIView
from rest_framework.response import Response
from .serializers import Upload_bannerSerializer
from django.core.serializers import serialize




def index (request):
    return render(request, 'front_end/index.html')


#



def upload_file_to_model(request):
    # form = pdf_Form(request.POST, request.FILES)
    pdf=Upload_pdf()
    pdf_metadata=Pdf_information()
    if request.method == 'POST':
             # pdf_type=form.save(commit=False)

            uploaded_pdf = request.FILES['myfile']
            fs = FileSystemStorage()
            name = fs.save(uploaded_pdf.name, uploaded_pdf)
            uploaded_file_url = fs.url(name)
            # print(uploaded_file_url)
            # f = (settings.MEDIA_ROOT + "//" + name)
            f = ( "media//" + name)

            f_base64= ("media//base64//" + name)
            f_base = ("base64//" + name)
             #  encrypt

            p_en = open(f, "rb").read()
            bytessw = b64encode(p_en)
            t_en = open(f_base64+'.txt', 'wb')
            t_en.write(bytessw)
            t_en.close()

            #  get metadata

            p_enss = open(f, "rb")
            parser = PDFParser(p_enss)
            doc_metadata = PDFDocument(parser)
            inf_meta_dict = doc_metadata.info
            data = dict(ChainMap(*inf_meta_dict))
            # save file encrypted and his name in spescific model
            pdf.pdf_file=f_base+'.txt'
            pdf.save()
            # save metadata in another model



            try:
                objs = Pdf_information(**data)

                objs.save()

                returnmsg = {"status_code": "You must be awesome, the download has been completed successfully ", }
            except Exception as e:
                 returnmsg = {"wrong": "Oops , An error occurred, you need to"
                                       ""
                                       " upload file not has any metadata strange ", }



            return render(request, 'front_end/upload_status.html', returnmsg)

    return render(request, 'front_end/upload_pdf.html', {
    })




from pathlib import Path

def preview_pdf_by_id(request,pdf_id):
    document_id=Upload_pdf.objects.filter(pk=pdf_id)
    document_all=Upload_pdf.objects.all()

    gg=""
    # print(settings.MEDIA_ROOT+"//"+document_id[0].pdf_file.name)
    try:
        f_txt = open(settings.MEDIA_ROOT+"//"+document_id[0].pdf_file.name, 'rb')
        encoded_string = f_txt.read()
        bytses = b64decode(encoded_string, validate=True)
        gg=settings.MEDIA_ROOT+"//"+document_id[0].pdf_file.name
        f_txt = open(settings.MEDIA_ROOT+"//"+document_id[0].pdf_file.name, 'wb')
        f_txt.write(bytses)
        # repr(f_txt)
        f_txt.close()

    except:

            pass
    # document_id.pdf_file="C://Users//user//Documents//d.pdf"
    context={
        'document_id': document_id,
        'document_all':document_all,
        "gg":str(gg)

    }
    return render(request, 'front_end/show_pdf.html', context)




def preview_pdf(request):

    document1=Upload_pdf.objects.all()
    return render(request, 'front_end/show_pdf.html', {'document1': document1})





def show_json_by_id(request,doc_id):
    document_id=Pdf_information.objects.filter(pk=doc_id)
    document_all=Pdf_information.objects.all()
    # serializer = Upload_bannerSerializer(document_id,many=True)
    # serial=serializer.data

    p=serialize('json', document_id)
    context={
        'document_id': document_id,
        'document_all': document_all,
        'serializer':p
    }


    return render(request, 'front_end/show_json.html', context)




def show_json(request):

    document1=Pdf_information.objects.all()
    return render(request, 'front_end/show_json.html', {'document1': document1})







