from django.urls import path
from django.conf import settings
from django.conf.urls import url
from django.urls import path
from difflib import SequenceMatcher
from django.conf.urls.static import static
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('upload', views.upload_file_to_model, name='upload_file_to_model'),
    path('pdf_preview/<pdf_id>', views.preview_pdf_by_id, name='preview_pdf_by_id'),
    path('pdf_preview', views.preview_pdf, name='preview_pdf'),
    path('json_represent/<doc_id>', views.show_json_by_id, name='show_json_by_id'),
    path('json_represent', views.show_json, name='show_json'),
]


if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)