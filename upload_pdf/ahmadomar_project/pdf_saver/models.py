from django.db import models

# Create your models here.
import os




class Pdf_information(models.Model):
    Title = models.CharField(max_length=100,blank=True , null =True)
    Author=models.CharField(max_length=100,blank=True , null =True)
    CreationDate=models.CharField(max_length=100,blank=True , null =True)
    Creator=models.CharField(max_length=100,blank=True , null =True)
    ModDate=models.CharField(max_length=100,blank=True , null =True)
    Producer=models.CharField(max_length=100,blank=True , null =True)
    ICNAppName=models.CharField(max_length=100,blank=True , null =True)
    ICNAppPlatform = models.CharField(max_length=100, blank=True, null=True)
    ICNAppVersion = models.CharField(max_length=100, blank=True, null=True)
    Trapped=models.CharField(max_length=100,blank=True , null =True)
    Keywords = models.CharField(max_length=100, blank=True, null=True)
    Subject = models.CharField(max_length=100, blank=True, null=True)



    def __str__(self):
        return str(self.Title)









class Upload_pdf(models.Model):
    # title = models.ForeignKey(Pdf_information, on_delete=models.CASCADE,blank=True,default="Unknown",null=True)
    # title = models.CharField(max_length=255, blank=True,default="Unknown",null=True)
    pdf_file = models.FileField(upload_to='base64/')

    uploaded_at = models.DateTimeField("When Created",editable=True, auto_now_add=True)

    # def __str__(self):
    #     return self.title

    def filename(self):
        return os.path.basename(self.pdf_file.name)



