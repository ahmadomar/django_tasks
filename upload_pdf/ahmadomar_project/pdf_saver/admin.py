from django.contrib import admin

# Register your models here.

from .models import Upload_pdf,Pdf_information


admin.site.register(Upload_pdf)

admin.site.register(Pdf_information)
